#Installation

##Dépendances
    
    - composer
    - npm
    - docker / docker-compose

###Front installation
    
    - $> cd front/
    - $> npm install

###Back installation
    
    - $> cd back/symfony/
    - $> composer install

#Utilisation
    
    - Installer le back et le front
    - $> docker-compose up
    - Le serveur front se trouve sur localhost:3000
    - Le serveur back se trouve sur localhost:20000
    - Adminer se trouve sur localhost:20001

##Connexion à la Db via adminer 
    - serveur : project-one-mariadb
    - utilisateur : root
    - mot de passe : root
    - base de données : Project_One_Db

#Recommandation
    
    - pour PHP coder avec les normes Psr-1 et Psr-2a